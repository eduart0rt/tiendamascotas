import pymysql
# 'pymysql' libreria necesaria para la conexion de Python a MYSQL

class Conexion_DB:
    def __init__(self):
            self.connection = pymysql.connect(
                host='localhost',
                database='daily_db',
                user='root',
                password='sena2021*' 
            ) 
            self.cursor = self.connection.cursor()

            print("Conexión establecida. Select de prueba de conexion:...")
            print("")
    
    def select_user(self, id):
            sql = 'SELECT id_usuario, nombre, apellidos, cedula, celular, mail FROM usuarios'.format(id)
            try:
                self.cursor.execute(sql)
                user = self.cursor.fetchone()

                print("Id: ", user[0])
                print("Nombre: ", user[1])
                print("Apellidos: ", user[2])
                print("Cedula: ", user[3])
                print("Celular: ", user[4])
                print("email: ", user[5])
            except Exception as e:
                raise
            

    def select_all_user(self):
            sql = 'SELECT id_usuario, nombre, apellidos, cedula, celular, mail FROM usuarios'   
            try:
                self.cursor.execute(sql)
                users = self.cursor.fetchall()
                for user in users:
                    print("Id: ", user[0])
                    print("Nombre: ", user[1])
                    print("Apellidos: ", user[2])
                    print("Cedula: ", user[3])
                    print("Celular: ", user[4])
                    print("email: ", user[5])
                    print("______________\n")
            except Exception as e:
                raise

    def update_user(self, id, username):
            sql = "UPDATE usuarios SET nombre='{}' WHERE id_usuario = {}".format(username, id)
            try:
                self.cursor.execute(sql)
                self.connection.commit()# Necesario si hay cambios en los datos, se confirma el cambio permanente.
                
                
            except Exception as e:
                raise
            
    def close(self):
            self.connection.close()
            print("Conexión cerrada...")
        
    


